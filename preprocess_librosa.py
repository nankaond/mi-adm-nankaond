#!/usr/bin/env python
# coding: utf-8


import os
import numpy as np
import librosa
import librosa.display
import pandas as pd
import matplotlib.pyplot as plt
from pathlib import Path



# ### Function to create spectograms


def create_spectogram(filepath):
    filename = filepath
    y, sr = librosa.load(filename)
    spect = librosa.feature.melspectrogram(y=y, sr=sr, n_fft=2048, hop_length=1024)
    spect = librosa.power_to_db(spect, ref=np.max)
    return spect.T


def plot_spect(filepath):
    spect = create_spectogram(filepath)
    print(spect.shape)
    plt.figure(figsize=(10, 4))
    librosa.display.specshow(spect.T, y_axis='mel', fmax=8000, x_axis='time')
    plt.colorbar(format='%+2.0f dB')
    plt.show()


def create_array(filepaths):
    genres = []
    X_spect = np.empty((0, 640, 128))
    count = 0
    genres = ['unknown']
    # Code skips records in case of errors
    for filepath in filepaths:
        try:
            count += 1

            spect = create_spectogram(filepath)

            # Normalize for small shape differences
            spect = spect[:640, :]
            X_spect = np.append(X_spect, [spect], axis=0)

            if count % 5 == 0:
                print("Currently processing: ", count)
        except MemoryError:
            print("Couldn't process: ", count)
            continue
    y_arr = np.array(genres)
    return X_spect, y_arr



def get_file_paths(directory):


    results = []
    for root, dirs, files in os.walk(directory):
        for file_ in files:
            filename = Path(root) / file_
            results.append(filename)

    return results



def process_dir(directory, output_name):
    print(directory)
    file_paths = get_file_paths(directory)
    X_conf, y_conf = create_array(file_paths)

    np.savez('test_arr', X_conf, y_conf)
    ### Convert the scale of training data
    X_conf_raw = librosa.core.db_to_power(X_conf, ref=1.0)

    X_conf_log = np.log(X_conf_raw)
    np.savez(output_name, X_conf_log, y_conf)



AUDIO_DIR = './cutted'

process_dir('ageispolis', 'ageispolis')
process_dir('ragga100', 'ragga100')
process_dir('police', 'police')
process_dir('devin', 'devin')