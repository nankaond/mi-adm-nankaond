Music genre classification/Similar mood tracks clustering


# Data

To run code in this repo you need

https://github.com/mdeff/fma small dataset and metadata

`fma_metadata`

`fma_small`

for baseline metadata based models it is enough to have the metadata
for preprocessing you need the raw mp3 from which spectrograms etc. are generated and saved to npz files
preprocessing takes a while preprocessed files are save on my google drive which is mounted to https://colab.research.google.com at runtime.
I chose to use this solution because I do not have a GPU available at the moment.

# Notes

Clustering based on similar dynamics and repetitions.

Smart feature engineering
Embedding

Convoltion neural network/ Recurrent neural network

Very nice preprocessed dataset with original audio files included.

https://github.com/mdeff/fma


Goal of the work could be to try a few different CNN architectures and then trying to achieve better results using ensemble model.
https://towardsdatascience.com/ensembling-convnets-using-keras-237d429157eb

Try to apply Transfer Learning?
## Konzultace 21.3.2019

Zkusit se zaměřit na explainability raději než na ensemble, vytáhnout z modelu/vygenerovat specifické 'haluz' zvuky, které hodně vystihují žánr. Je možné zkusit místo spektrogramu použít raw signál.

https://christophm.github.io/interpretable-ml-book/

https://www.youtube.com/watch?v=DGGpHARTNUE

### Convultion filters visualization
https://jacobgil.github.io/deeplearning/filter-visualizations

http://cs231n.github.io/understanding-cnn/

http://benanne.github.io/2014/08/05/spotify-cnns.html

https://groups.google.com/forum/#!topic/librosa/E1RvuRB8aDI

https://towardsdatascience.com/visualizing-intermediate-activation-in-convolutional-neural-networks-with-keras-260b36d60d0

https://github.com/raghakot/keras-vis


## Konzultace 2.5.2019

Report
zminit google collab
zminit dekonvoluci pro ucely explainability
za ucelem explainability zkusit vybrat par songu, ktere se nachazi na pomezi zanru a klasifikovat sliding window z techto zanru a pozorovat jak se postupne meni vystup klasifikace

